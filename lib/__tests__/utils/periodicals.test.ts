import { Periodical } from '../../../lib/interfaces/Periodical';
import { Session } from '../../../lib/interfaces/Session';
import { isArticleAuthor, isPeriodicalOwner, isPublic } from '../../utils/periodicals';

describe('isPublic', () => {
  const mockPeriodical = {
    creator: { identifier: 'Arbitrary session ID' },
    datePublished: 'arbitrary date',
    name: 'Arbitrary journal name',
  };
  it('should return true when all requirements for public periodicals have been fulfilled', () => {
    expect(isPublic(mockPeriodical)).toBe(true);
  });

  it('should return false when a periodical has no publication date yet', () => {
    const datelessPeriodical = {
      ...mockPeriodical,
      datePublished: undefined,
    };

    expect(isPublic(datelessPeriodical)).toBe(false);
  });

  it('should return false when a periodical has no name yet', () => {
    const namelessPeriodical = {
      ...mockPeriodical,
      name: undefined,
    };

    expect(isPublic(namelessPeriodical)).toBe(false);
  });

  it('should return false when a periodical has no owner yet', () => {
    const ownerlessPeriodical = {
      ...mockPeriodical,
      creator: undefined,
    };

    expect(isPublic(ownerlessPeriodical)).toBe(false);
  });
});

describe('isPeriodicalOwner', () => {
  it('should return true when the given account owns the given Journal', () => {
    const mockPeriodical = {
      creator: { identifier: 'Some session ID' },
      identifier: 'Arbitrary Journal ID',
    };
    const mockSession: Session = { account: { identifier: 'Some session ID' }, identifier: 'Arbitrary session ID' };

    expect(isPeriodicalOwner(mockPeriodical, mockSession)).toBe(true);
  });

  it('should return false when no account is linked to this session', () => {
    const mockPeriodical = {
      creator: { identifier: 'Arbitrary account ID' },
      identifier: 'Arbitrary Journal ID',
    };
    const mockSession: Session = {
      identifier: 'Arbitrary session ID',
    };

    expect(isPeriodicalOwner(mockPeriodical, mockSession)).toBe(false);
  });
});

describe('isArticleAuthor', () => {
  it('should return true when the given account is one of the article authors', () => {
    const mockArticle = {
      author: [
        { identifier: 'Arbitrary other author ID' },
        { identifier: 'Some account ID' },
      ],
      identifier: 'Arbitrary article ID',
    };
    const mockSession: Session = { account: { identifier: 'Some account ID' }, identifier: 'Arbitrary session ID' };

    expect(isArticleAuthor(mockArticle, mockSession)).toBe(true);
  });

  it('should return false when no account is linked to this session', () => {
    const mockArticle = {
      author: [
        { identifier: 'Arbitrary other author ID' },
        { identifier: 'Some account ID' },
      ],
      identifier: 'Arbitrary article ID',
    };
    const mockSession: Session = {
      identifier: 'Arbitrary session ID',
    };

    expect(isArticleAuthor(mockArticle, mockSession)).toBe(false);
  });
});
