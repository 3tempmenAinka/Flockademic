import {
  PostRefreshJwtRequest,
  PostRefreshJwtResponse,
} from '../../../../lib/interfaces/endpoints/accounts';
import { Session } from '../../../../lib/interfaces/Session';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { signJwt } from '../../../../lib/lambda/signJwt';
import { getSessionForToken } from '../dao';

export async function getNewJwt(
  context: Request<PostRefreshJwtRequest> & DbContext,
): Promise<PostRefreshJwtResponse> {
  if (!context.body || !context.body.refreshToken) {
    throw new Error('Please provide a refresh token to receive a new JWT.');
  }

  let session: Session;
  try {
    session = await getSessionForToken(context.database, context.body.refreshToken);
  } catch (e) {
    throw new Error('Something went wrong, please try again.');
  }

  if (!context.params || context.params.length < 2) {
    throw new Error('No user ID specified.');
  }
  const sessionId = context.params[1];

  if (sessionId !== session.identifier) {
    throw new Error('The refresh token was not valid.');
  }

  const jwt = signJwt(session);

  if (jwt instanceof Error) {
    throw new Error('We are currently experiencing issues, please try again later.');
  }

  return { jwt };
}
