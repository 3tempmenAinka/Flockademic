import * as PropTypes from 'prop-types';

export const mockRouterContext = {
  childContextTypes: { router: PropTypes.object },
  context: { router:  {
    history: {
      createHref: () => undefined,
      push: () => undefined,
      replace: () => undefined,
    },
    route: {
      location: 'arbitrary_location',
      match: undefined,
    },
  } },
};
