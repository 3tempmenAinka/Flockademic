import { GetPeriodicalResponse } from '../../../../lib/interfaces/endpoints/periodical';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { SessionContext } from '../../../../lib/lambda/middleware/withSession';
import { fetchPeriodical as commandHandler } from '../services/periodical';

export async function fetchPeriodical(
  context: Request<undefined> & DbContext & SessionContext,
): Promise<GetPeriodicalResponse> {
  if (!context.params || context.params.length < 2) {
    throw(new Error('Could not find a journal without a journal ID'));
  }

  const session = (context.session instanceof Error) ? undefined : context.session;

  try {
    const periodical = await commandHandler(context.database, context.params[1], session);

    // The command handler might return private data (such as session keys of the creator),
    // so only return required values.
    return {
      creator: periodical.creator,
      datePublished: periodical.datePublished,
      description: periodical.description,
      headline: periodical.headline,
      identifier: periodical.identifier,
      image: periodical.image,
      name: periodical.name,
    };
  } catch (e) {
    throw new Error(`Could not find a Journal with ID \`${context.params[1]}\`.`);
  }
}
