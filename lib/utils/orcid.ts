export function isOrcidWorkLink(link: string) {
  const orcidUrlRegex = /^https:\/\/orcid.org\/\d{4}\-\d{4}\-\d{4}\-\d{3}(\d|X)\/work\/\d+$/;

  return orcidUrlRegex.test(link);
}

export function isOrcidWorkId(id: string) {
  const orcidIdRegex = /^\d{4}\-\d{4}\-\d{4}\-\d{3}(\d|X):\d+$/;

  return orcidIdRegex.test(id);
}

export function convertToOrcidWorkLink(orcidWorkId: string) {
  const [ orcid, putCode ] = orcidWorkId.split(':');

  return `https://orcid.org/${orcid}/work/${putCode}`;
}

export function convertToOrcidWorkId(orcidWorkLink: string) {
  // Links to ORCID works are of the form `https://orcid.org/<orcid>/work/<putCode>`.
  const orcid = orcidWorkLink.substr('https://orcid.org/'.length, 19);
  const putCode = orcidWorkLink.substr('https://orcid.org/'.length + 19 + '/work/'.length);

  return `${orcid}:${putCode}`;
}

export function convertToOrcidLink(orcid: string) {
  return 'https://orcid.org/' + orcid;
}

export function convertToOrcid(orcidLink: string) {
  // Links to ORCID profiles are of the form  `https://orcid.org/<orcid>`
  return orcidLink.substr('https://orcid.org/'.length, 19);
}
