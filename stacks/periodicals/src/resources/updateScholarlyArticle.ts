import {
  PostUpdateScholarlyArticleRequest,
  PostUpdateScholarlyArticleResponse,
} from '../../../../lib/interfaces/endpoints/periodical';
import { ScholarlyArticle } from '../../../../lib/interfaces/ScholarlyArticle';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { SessionContext } from '../../../../lib/lambda/middleware/withSession';
import { isArticleAuthor } from '../../../../lib/utils/periodicals';
import {
  scholarlyArticleDescriptionValidators,
  scholarlyArticleNameValidators,
} from '../../../../lib/validation/scholarlyArticle';
import { isValid } from '../../../../lib/validation/validators';
import { fetchScholarlyArticle } from '../services/scholarlyArticle';
import { updateScholarlyArticle as service } from '../services/updateScholarlyArticle';

export async function updateScholarlyArticle(
  context: Request<PostUpdateScholarlyArticleRequest> & DbContext & SessionContext,
): Promise<PostUpdateScholarlyArticleResponse> {
  if (!context.body || !context.body.object) {
    throw new Error('Please specify properties to update.');
  }
  const proposedArticle: Partial<ScholarlyArticle> = context.body.object;

  if (context.session instanceof Error) {
    throw context.session;
  }

  if (!context.body.targetCollection || !context.body.targetCollection.identifier) {
    throw(new Error('No article to update specified'));
  }

  const storedArticle =
    await fetchScholarlyArticle(context.database, context.body.targetCollection.identifier, context.session);

  if (storedArticle === null) {
    throw new Error(`Could not find an article with ID ${context.body.targetCollection.identifier}.`);
  }

  const articleAuthors = storedArticle.author || undefined;
  if (
      storedArticle.creatorSessionId !== context.session.identifier
      && (!articleAuthors || !isArticleAuthor({ author: articleAuthors }, context.session))
   ) {
    throw new Error('You can only update articles you are an author of.');
  }

  const toUpdate: {
    description?: string;
    name?: string;
  } = {};
  if (isValid(proposedArticle.description, scholarlyArticleDescriptionValidators)) {
    toUpdate.description = proposedArticle.description;
  }
  if (proposedArticle.name && isValid<string>(proposedArticle.name, scholarlyArticleNameValidators)) {
    toUpdate.name = proposedArticle.name;
  }

  try {
    const result = await service(
      context.database,
      context.body.targetCollection.identifier,
      toUpdate,
    );

    return {
      result: {
        description: result.description,
        name: result.name,
      },
      targetCollection: {
        identifier: context.body.targetCollection.identifier,
      },
    };
  } catch (e) {
    throw new Error('There was a problem modifying the article, please try again.');
  }
}
