import fetch from 'node-fetch';
import { v4 as uuid } from 'uuid';

import { PostVerifyOrcidRequest, PostVerifyOrcidResponse } from '../../../../lib/interfaces/endpoints/accounts';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { signJwt } from '../../../../lib/lambda/signJwt';
import { verifyJwt } from '../../../../lib/lambda/verifyJwt';
import { getAccountForOrcid, storeOrcidCredentials } from '../dao';

export async function linkOrcid(
  context: Request<PostVerifyOrcidRequest> & DbContext,
): Promise<PostVerifyOrcidResponse> {
  if (!context.params || context.params.length < 3) {
    throw new Error('Please specify both a session ID and an ORCID verification code.');
  }
  const session = verifyJwt(context.body.jwt);

  if (session instanceof Error) {
    return Promise.reject(new Error('Something went wrong linking your session to your ORCID, please try again.'));
  }
  const parameters = `client_id=${process.env.orcid_client_id}`
      + `&client_secret=${process.env.orcid_client_secret}`
      + `&code=${context.params[2]}`
      + '&grant_type=authorization_code'
      + `&redirect_uri=${context.body.redirectUri}`;

  const fetchOptions = {
    body: parameters,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    method: 'POST',
    redirect: 'follow' as 'follow',
  };
  const fetchUrl = `${process.env.orcid_base_path}/oauth/token`;

  const response = await fetch(fetchUrl, fetchOptions);
  const dataOrError = await response.json();

  if (typeof dataOrError.error !== 'undefined') {
    // Unfortunately, TypeScript isn't able to discern between union types,
    // so `dataOrError` is still of type any. See https://stackoverflow.com/q/46468882/859631
    // If you happen to want to know the structure of error messages, it's:
    // const error = dataOrError as { error: string; error_description?: string; };

    throw new Error('Your ORCID could not be confirmed, please try again.');
  }

  // Unfortunately, TypeScript isn't able to discern between union types, so
  // `dataOrError` is still of type any and can only be coerced into the correct type here.
  // See https://stackoverflow.com/q/46468882/859631
  const data = dataOrError as {
      access_token: string;
      token_type: string;
      refresh_token: string;
      expires_in: number;
      scope: string;
      orcid: string;
      name?: string;
    };

  const account = await getAccountForOrcid(context.database, data.orcid);
  const linkedAccountId = session.account ? session.account.identifier : undefined;

  if (linkedAccountId && account && linkedAccountId !== account.identifier) {
    return Promise.reject(new Error('This ORCID is already associated with another account.'));
  }

  // If we found an account linked to this ID, link the current session to that one.
  // Otherwise, if an account is already linked to the current session, use that one.
  // Otherwise, generate a UUID for a new account.
  const accountId = (account) ? account.identifier : linkedAccountId || uuid();

  try {
    await storeOrcidCredentials(
      context.database,
      data.orcid,
      data.access_token,
      data.refresh_token,
      session.identifier,
      accountId,
      data.name,
    );
  } catch (e) {
    throw new Error('Something went wrong verifying your ORCID, please try again.');
  }

  const newToken = signJwt({
    account: {
      identifier: accountId,
      orcid: data.orcid,
   },
    identifier: session.identifier,
  });

  if (newToken instanceof Error) {
    throw new Error('We are currently experiencing issues, please try again later.');
  }

  return {
    account: {
      identifier: accountId,
      orcid: data.orcid,
    },
    jwt: newToken,
  };
}
